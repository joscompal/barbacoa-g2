== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre


* Comitre Palacios, José Joaquín

* Luque Giráldez, José Rafael


* Navas Gutiérrez, Ángeles


* Piedra Venegas,Enrique


* Perez Lopez, Sergio
* Romero Sanchez, Pablo


=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Citroen Xsara  (5 plazas)

* Luque Giráldez, José Rafael

* Piedra Venegas,Enrique



==== Skoda Fabia (5 plazas)

* Navas Gutiérrez, Ángeles


==== Peugeot 308 Allure (5 plazas)
 
* Perez Lopez, Sergio
* Peralvo German, Marcos Stephan


==== Opel Meriva (5 plazas)

* Pablo Romero Sanchez




==== Renault Clio (5 plazas)

* Comitre Palacios, José Joaquín

